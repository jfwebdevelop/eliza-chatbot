const express = require('express');

const bodyParser = require('body-parser');

const cors = require('cors');

const serve = require('express-static');

const app = express();


//----------------------------------------------------

const config = require('./config.js');

const chat_routes = require('./routes/chats.js');



//---------------------------------------------------------------------------
//set view engine
app.set('view engine', 'ejs');
// to support JSON-encoded bodies
app.use( bodyParser.json() );
// to support URL-encoded bodies
app.use( bodyParser.urlencoded({ extended: true }) );

//---------------------------------------------------------------------------
// CORS (Cross-Origin Resource Sharing) to support Cross-Site HTTP requests

app.use(cors(config.corsOptions));

//---------------------------------------------------------------------------


app.get('/', function(req, res){
  res.render(__dirname + '/public/index');
});

app.get('/impressum', function(req, res){
   res.render(__dirname + '/public/partials/impressum');
});

app.post('/welcome', chat_routes.welcomeMessage);

app.post('/newQuestion', chat_routes.newQuestion);


//---------------------------------------------------------------------------

// Static serve /public
app.use('/', serve(__dirname + '/public'));

//---------------------------------------------------------------------------

app.set('port', config.expressConfig.port);

app.listen(app.get('port'), function () {
  console.log('Express Server listening on port ' + app.get('port'));
});