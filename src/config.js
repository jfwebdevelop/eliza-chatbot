
module.exports = {

    expressConfig: {
        port : 3000
    },

    mongo: {
        host: 'mongodb+srv://jonas:dyIfuCW4oIX4uwMH@eliza-w7eck.mongodb.net/admin',
        database: 'elizaData',
        useNewUrlParser: true
    },

    corsOptions: {
        origin: '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Content-Type', 'Access-Control-Allow-Headers', 'Authorization', 'X-Requested-With']
    }
};
