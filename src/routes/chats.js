//--------------------------------------------------------------
// Eliza Daten einbinden
const data = require('../lib/elizaData.js');
// Node Caching Package um Eliza Daten temporär zu speichern
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();
//--------------------------------------------------------------
// Fallback Matching Case
const defaultNone = 'xnone';
// Goto Case um in anderen Zerlegungsmuster zu wechseln
const goto = 'goto';


//                                          ELIZA FUNCTIONS
//----------------------------------------------------------------------------------------------------------------------

// Hauptfunktion zum Keyword Matching Prozess
function matchKeyword(question, searchword, newGoto=false) {

    let foundSth = false;
    let changeRegExp = false;
    let results = [];
    for (index in data.keywords) {
        let keyword = data.keywords[index][0];

        // Wenn Daten gecached wurden bei nächsten Default Case mit diesen Daten ein Matching beginnen und Cache löschen (Erinnerungseffekt des Chatbots)
        if(searchword === defaultNone) {
            if(myCache.get('tempXnone') !== undefined) {
                question = myCache.get('tempXnone').question;
                searchword = myCache.get('tempXnone').searchword;
                changeRegExp = true;
                // tempXnone Cache löschen
            }
        }

        //---------------------------------*
        // überprüfen ob das eingegebene Wort einem Schlüsselwort aus dem Datenbestand entspricht
        if(searchword === keyword ) {
            /*
            Loop for getting into further array
            [["* can you *", []]], ==> ["* can you *", []]
             */
            data.keywords[index][2].forEach(element => {
                let preRegExp = element[0];

                // Wenn $ Case für Speicherung des Matchings eintritt, Daten im Cache speichern
                let preRegExpSave = preRegExp.match(new RegExp('[$]'));
                if(preRegExpSave !== null && changeRegExp) {
                    // Zerlegungsmuster für regExp Matching Case Vorbereiten
                    preRegExp = preRegExp.replace('$ ','');
                }

                if(preRegExpSave !== null) {
                    if(myCache.get('tempXnone') === undefined) {
                        // console.log(myCache.get('tempXnone'));
                        let obj = { question: question, searchword: searchword};
                        // tempXnone Cache setzen
                        myCache.set('tempXnone', obj, function(err, success) {
                            if(!err && success) {
                                // console.log(success);
                            }
                        });
                    }
                }
        //---------------------------------*


        //---------------------------------*
        // Zerlegungsmuster  nach @Synonym durchsuchen
                let regExpSynom = new RegExp('[\\@]+(\\w+)','g');

                if((regExpSynom.test(element[0]))) {
                    let synomPlaceholder = element[0].match(regExpSynom);
                    let questionPattern = preparePattern(element[0]);
                    let synonymReplacement = replaceSynonym(questionPattern, question);

                    if(synonymReplacement !== undefined) {

                        preRegExp = preRegExp.replace(synomPlaceholder[0], synonymReplacement);

                    }

                }
        //---------------------------------*

        //---------------------------------*
                // Regulären Ausdruck durch Aufrufen der preparePattern Funktion vorbereiten
                let finalRegExp = preparePattern(preRegExp);
                let prio = data.keywords[index][1];
                let length =  element[1].length;

                // Space wird für die richtige Ausführung des regulären Ausdrucks benötigt, da sonst in bestimmten Fällen kein Matching stattfindet
                question = question + ' ';
                let found =  question.match(finalRegExp);
                if(found !== null) {
                    let preAnswer = element[1][Math.floor(Math.random()*length)];

                    // (NUMMER) durch $NUMMER ersetzen
                    let finalAnswer = preAnswer.replace('(', '$').replace(')', '');

                    // Daten im Results Array speichern
                    results.push({
                        originQuestion: question,
                        regularExp: finalRegExp,
                        answer: finalAnswer,
                        matchingResults: found,
                        priority: prio
                    });
                    // Found Something Flag auf true setzen
                    foundSth = true;
                }
        //---------------------------------*
            });
        }
    }

    return foundSth ? results : false;
}


// Funktion um regulären Ausdruck in Zerfallsmuster vorzubereiten
function preparePattern(preRegExp) {

    let regexpSpace = new RegExp('(\\S)\\s*\\*\\s*(\\S)', 'g');

    let regExpSpaceMatch = preRegExp.match(regexpSpace);
    if(regExpSpaceMatch) {
        let regExpSpaceMatchFinal = regExpSpaceMatch[0].replace('*','*?');
        preRegExp = preRegExp.replace(regExpSpaceMatch[0], regExpSpaceMatchFinal);
    }
    // Space hinter Stern von Eliza Zerlegungsmuster entfernen
    preRegExp = preRegExp.replace(new RegExp('^[\*] ','g'),'*');
    let test = new RegExp('\\*(?!\\?)', 'g');

    let testregexp = preRegExp.replace(test,'(.*)');

    let finalRegExp = testregexp.replace(new RegExp('\\*\\?', 'g'), '(.*)');


    return finalRegExp;

}

// Funktion um Synonyme des @ Zeichens durch das richtige zu verwendene Synonym zu ersetzen
function replaceSynonym(pattern, question) {

    let synonymWordPattern = new RegExp('@([^\\s]+)', 'g');
    let synonymWord = pattern.match(synonymWordPattern)[0];
    synonymWord = synonymWord.split('@')[1];

    let synonymArray = data.elizaSynons[synonymWord];

    let regExpSynonymString = '';
    let regExpSynonymArray = synonymArray.forEach(synonym => {

        regExpSynonymString = regExpSynonymString+synonym+'|';

    });
    regExpSynonymString = regExpSynonymString.slice(0, -1);
    regExpSynonymString = '  |'+regExpSynonymString +'|  ';
    let finalSynonymPattern = pattern.replace(synonymWordPattern, regExpSynonymString);
    let synonymReg = new RegExp(finalSynonymPattern, 'g');

    let questionMatch = question.match(synonymReg);

    if(questionMatch) {
        return '(' + questionMatch[0] + ')';
    }


}

// Pronomen in das Gegenteilige von Input zu Output übersetzen (you wird beispielsweise zu me)
function replacePronouns(regExpVar) {
    regExpVar = regExpVar.replace(/\?/g,'');
    let filterRegExpVar = regExpVar.split(' ');
    let pronoun = filterRegExpVar;

    let pronounArray = data.elizaPronouns;
    for(let i=0; i<pronounArray.length; i+=2) {
        let pronounResult = false;
        for(let j = 0; j<pronoun.length; j++) {
            if(pronoun[j] === pronounArray[i] && pronounResult !== true) {
                pronounResult = true;
                let matchPronoun = pronounArray[i+1];
                regExpVar = regExpVar.replace(' '+ pronoun[j]+ ' ', ' ' + matchPronoun + ' ');
            }
        }

    }
    return regExpVar;
}

// Funktion um ähnliche Wörter in den gleichen Case matchen
function replaceEqualWords(question) {
    let questionArray = question.split(' ');
    for(let i=0; i < data.elizaPres.length; i+=2) {
        let equalWordResult = false;
        for(let j = 0; j<questionArray.length; j++) {
            if(data.elizaPres[i] === questionArray[j] && equalWordResult !== true) {
                equalWordResult = true;
                let matchEqualWord = data.elizaPres[i+1];
                question = question.replace(questionArray[j], matchEqualWord);
            }
        }
    }
    return question;


}

// Funktion um nachträglich Antworten anzupassen, die aus einem zu kurzen Antwortsatz bestehen
function postTransformAnswer(customAnswer) {
    customAnswer= customAnswer.replace(/\s{2,}/g, ' ');

    customAnswer=customAnswer.replace(/\s+\./g, '.');

    if ((data.elizaPostTransforms) && (data.elizaPostTransforms.length)) {
        for (var i=0; i<data.elizaPostTransforms.length; i+=2) {
            customAnswer=customAnswer.replace(data.elizaPostTransforms[i], data.elizaPostTransforms[i+1]);
            data.elizaPostTransforms[i].lastIndex=0;
        }
    }
    // Erstes Zeichen groß setzen
    if (this.capitalizeFirstLetter) {
        var regExp=/^([a-z])/;
        var message =regExp.exec(customAnswer);
        if (message) customAnswer=message[0].toUpperCase()+customAnswer.substring(1);
    }
    return customAnswer;
}



//----------------------------------------------------------------------------------------------------------------------

exports.newQuestion = function(req, res) {

    let question = req.body.question.toLowerCase();
    question = question.replace(new RegExp('\\.+$'),'');
    question = replaceEqualWords(question);
    let questionArray =  question.split(' ');



//                                          ELIZA QUITS
    //------------------------------------------------------------------------------------------------------------------
    let elizaQuits = data.elizaQuits;
    let quitMessages = data.elizaFinals;
    let randomQuitMessage = quitMessages[Math.floor(Math.random()*quitMessages.length)];
    for(var i = 0; i<elizaQuits.length; i++) {
        if(questionArray.indexOf(elizaQuits[i]) >= 0) {
            res.send({answer: randomQuitMessage, all: data.keywords});
            return;
        }
    }
    //------------------------------------------------------------------------------------------------------------------


    //                                      MATCHING CASES
    //------------------------------------------------------------------------------------------------------------------
    let allResults = [];
    let allPriorities = [];


    // Keyword Matching Funktion aufrufen und einzelne Wörter des Satzes mit Keywords abgleichen
    questionArray.forEach((word) => {
        let results = matchKeyword(question,word);
        if(results) allResults.push(results);
    });
    // ------------------

    // FALLBACK MATCHING wenn kein Schlüsselwort gefunden wird
    if(allResults.length === 0) {

        myCache.get('tempXnone', function(err, value) {
            if (!err) {
                // console.log('here:'+value);
                if(value === undefined) {
                    // Standard Fallback Matching ohne Cache Rückgriff
                    allResults = [matchKeyword(question,defaultNone)];
                } else {
                    // Erweitertes Fallback Matching mit Cache Rückgriff
                    let defaultNone = 'xnone';
                    let question = value.question;
                    allResults = [matchKeyword(question,defaultNone)];
                    // console.log(allResults);
                    myCache.del( "tempXnone" );
                }
            }
        });

    }

    //-------------------

    // Antwortsatz und Ausgangsfrage aus dem zurückgeebenen Results Array speichern
    let resultAnswer = allResults[0][0].answer;
    let resultQuestion = allResults[0][0].regularExp;
    //-------------------

    // Wenn mehrere Ergebnisse zurückkommen wird hier die höchste Priorität ausgewählt und die daraus resultierende Antwort und Frage eingesetzt
    if(allResults.length >= 2) {
        for(let i = 0; i < allResults.length; i++) {
            var priorities = allResults[i][0].priority;
            allPriorities.push(priorities);
        }
        let majorPriority = Math.max.apply(Math, allPriorities);
        for(let i = 0; i < allResults.length; i++) {
            if(allResults[i][0].priority == majorPriority) {
                resultAnswer = allResults[i][0].answer;
                resultQuestion = allResults[i][0].regularExp;
            }
        }
    }

    // -----------------

    // GOTO MATCHING, wenn goto in Zerlegungsmuster entdeckt wird erneutes Keyword Matching mit goto Schlüsselwort durchführen
    let checkKeywordOperator = resultAnswer.search(goto);
    if(checkKeywordOperator !== -1) {
        let answerReplacement = resultAnswer.replace(goto,'');
        let searchword = answerReplacement.replace(/\s/g,'');
        let newGoto = searchword;
        allResults = [matchKeyword(question, searchword, newGoto)];
        resultAnswer = allResults[0][0].answer;
    }
    //------------------------------------------------------------------------------------------------------------------

    //                                      ANSWER REPLACEMENTS
    //------------------------------------------------------------------------------------------------------------------
    // Vorberereitungen auf Zurückschicken der Serverantwort

    // Wenn die Antwortsätze ein Dollar Zeichen erhalten, Wildcard Replacements durchführen
    if(resultAnswer.indexOf('$') > -1) {
        let finalRegExpVar = resultAnswer.match(new RegExp('[\\$]+[1-9]', 'g'))[0];

        question = allResults[0][0].originQuestion;
        let regExpVar = question.replace(new RegExp(resultQuestion), finalRegExpVar);
        regExpVar = regExpVar.split(new RegExp('(\\bbut|and\\b|[.,?!])'))[0];


        // Pronoun Replacement Funktion aufrufen

        regExpVar = replacePronouns(regExpVar);

        let customAnswer = resultAnswer.replace(finalRegExpVar, regExpVar);

        //Für Fälle bei kurzen Antworten, wie "my boyfriend" --> "Is it important to you that your boyfriend"
        customAnswer = postTransformAnswer(customAnswer);
        res.send({answer: customAnswer, all: data.keywords});
    } else {
        res.send({answer: resultAnswer, all: data.keywords});
    }


    //------------------------------------------------------------------------------------------------------------------

};


//                                          ELIZA WELCOME
//----------------------------------------------------------------------------------------------------------------------
exports.welcomeMessage = function(req, res) {
    let welcomeMessageData = data.elizaInitials;
    let randomWelcomeMessage = welcomeMessageData[Math.floor(Math.random()*welcomeMessageData.length)];
    res.send({answer: randomWelcomeMessage});
};