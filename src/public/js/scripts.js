
// Full trim (inner trim)

String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};
//----------------------------------------------------------




const QUESTION_URL = 'http://localhost:3000/newQuestion';
const WELCOME_URL = 'http://localhost:3000/welcome';
//----------------------------------------------------------

const button = document.querySelector('#sendButton');
const input = document.querySelector('#question');
const chat = document.querySelector('#chat');

let locationUrl = window.location.pathname;
// const initialData = require('../../lib/elizaData');

if(locationUrl !== '/impressum') {
    button.addEventListener('click', sendNewQuestion);

      input.addEventListener('keydown', event => {
        if(input.value !== "") if (event.which == 13 || event.keyCode == 13)

          sendNewQuestion();

      });

    window.onload = function() {

        sendWelcomeMessage();

    };
    //----------------------------------------------------------

    function sendNewQuestion() {
      let questionValue = input.value;
      questionValue = questionValue.fulltrim();
      // console.log(questionValue);

      addMessage(questionValue);
      input.value = '';

      axios.post(QUESTION_URL, {
        question: questionValue,
      }).then(result => {
        if(result.data.answer) {
          addMessage(result.data.answer, true);
          chat.scrollTop = chat.scrollHeight - chat.clientHeight;
          input.value = '';
        }
        //chat.value += result.data.answer + '\n';



      }).catch(err => {
        console.log(err);
      });


    }

    function sendWelcomeMessage() {
        axios.post(WELCOME_URL)
            .then(result => {
            if(result.data.answer) {
                addWelcomeMessage(result.data.answer);
                chat.scrollTop = chat.scrollHeight - chat.clientHeight;
            }
            }).catch(err => {
            console.log(err);
        });
    }

    function addMessage(message, fromEliza=false) {
      if (fromEliza) {
        chat.innerHTML += '<p class="bot-message">ELIZA: ' + message + '</p>';
      } else {
        chat.innerHTML += '<p class="my-message">YOU: ' + message + '</p>';
      }
    }

    function setText() {
        if(input.value !== ""){
            button.disabled = false;
        }
        else{
            button.disabled = true;
        }
    }

    function addWelcomeMessage(message) {
        chat.innerHTML += '<p class="bot-message">ELIZA: ' + message + '</p>';
    }

    $(window).on("load",function() {
        let windowWidth;
        if($(window).width() < 768) {
            windowWidth = 400;
        } else {
            windowWidth = 0;
        }
        $(window).scroll(function() {
            let windowBottom = $(this).scrollTop() + $(this).innerHeight();
            $(".fade").each(function() {
                /* Check the location of each desired element */


                let objectBottom = $(this).offset().top + $(this).outerHeight() - windowWidth;

                if (objectBottom < windowBottom) {
                    if ($(this).css("opacity")==0) {
                        $(this).fadeTo(500,1);
                    }
                }
            });
        }).scroll(); //invoke scroll-handler on page-load
    });
} else {

    let navpoints = $('.mainnav ul li');
    let backlink = $('.mainnav ul .backlink');
    navpoints.css('display', 'none');
    backlink.css('display', 'inherit');

}

$(document).ready(function() {
        $("#chatbotAnchor").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#chatbot").offset().top
            }, 1000);
        });

        document.querySelectorAll('a[href^="#"]').forEach(anchor => {
            anchor.addEventListener('click', function (e) {
                e.preventDefault();

                document.querySelector(this.getAttribute('href')).scrollIntoView({
                    behavior: 'smooth'
                });
            });
        });

        let d = new Date();
        document.getElementById('copyright-year').innerHTML = d.getFullYear();
});



