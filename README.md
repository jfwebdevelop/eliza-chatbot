# chatbot-eliza

Ein Chatbot nach Eliza Funktionalität...


### Installation

1. Node Pakete installieren: <code>npm install</code>
2. Express Server starten: <code>node server.js</code>


### Anwendung erweitern:
- Daten können in der <em>elizaData.js</em> erweitert werden
<br>
<br>
<strong>Demo:</strong> [www.eliza-chatbot.de](http://www.eliza-chatbot.de)